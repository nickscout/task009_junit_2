import com.epam.nickscout.controller.Board;
import com.epam.nickscout.model.cell.CellType;
import com.epam.nickscout.model.cell.EmptyCell;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class MainTest {
    public static final Logger logger = LogManager.getLogger();
    public static Board board = Board.getInstance();

    @Test
    public void testMinesCount() {
        board.emptyCellStream().forEach(cell -> {
                    int count = cell.getCount();
                    int expectedCount = (int) cell.getNeighbours().stream()
                            .filter(n -> n.isPresent())
                            .filter(n -> n.get().cheatyPeek() == CellType.MINE)
                            .count();
                    assertEquals(count, expectedCount);
                });
    }

    @Test
    public void testBorderNeighboursAmount() {
        board.cellStream().forEach(cell -> {
            int i = cell.getRow();
            int j = cell.getColumn();

            int ROWS = board.getROWS() - 1;
            int COLUMNS = board.getCOLUMNS() - 1;

            int expectedNeigboursAmount;
            int actualNeigboursAmount = cell.getNeighbours().size();

            if (i == 0 && j == 0) {
                expectedNeigboursAmount = 3;
            } else if (i == 0 && j == COLUMNS) {
                expectedNeigboursAmount = 3;
            } else if (i == ROWS && j == 0) {
                expectedNeigboursAmount = 3;
            } else if (i == ROWS && j == COLUMNS) {
                expectedNeigboursAmount = 3;
            } else if (i == 0 || i == ROWS) {
                expectedNeigboursAmount = 5;
            } else if (j == 0 || j == COLUMNS) {
                expectedNeigboursAmount = 5;
            } else {
                expectedNeigboursAmount = 8;
            }
            assertEquals(expectedNeigboursAmount, actualNeigboursAmount);

        });
    }
}
