package com.epam.nickscout;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.nickscout.view.Console;


public class Main {
    public static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Console.play();
    }
}
