package com.epam.nickscout.controller;

import com.epam.nickscout.model.cell.Cell;
import com.epam.nickscout.model.cell.CellType;
import com.epam.nickscout.model.cell.EmptyCell;
import com.epam.nickscout.model.cell.MineCell;
import com.sun.xml.internal.ws.developer.StreamingAttachment;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Stream;

public  class Board extends ArrayList<ArrayList<Cell>> {
    @Getter
    private final int ROWS;
    @Getter
    private final int COLUMNS;
    private final int MINE_APPEARANCE_CHANCE = 25;
    private int score;
    private boolean isDead;

    private static Board ourInstance = new Board(20,20);

    public static Board getInstance() {
        return ourInstance;
    }

    public Board(int ROWS, int COLUMNS) {
        super(ROWS);
        this.ROWS = ROWS;
        this.COLUMNS = COLUMNS;
        score = 0;
        isDead = false;
        createMatrix();
        tieUpNeighbours();
        countAllNeibourMines();
    }

    private void createMatrix() {
        Random random = new Random();
        for (int i = 0; i < ROWS; i++) {
            ArrayList<Cell> row = new ArrayList<>(COLUMNS);
            for (int j = 0; j < COLUMNS; j++) {
                Cell cell;
                if (random.nextInt(100) < MINE_APPEARANCE_CHANCE) {
                    cell = new MineCell(i, j);
                } else {
                    cell = new EmptyCell(i, j);
                }
                row.add(cell);
            }
            add(row);
        }
    }

    private void tieUpNeighbours() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                Cell centerCell = get(i).get(j);
                tieUpNeighbours(centerCell);
            }
        }
    }

    private void tieUpNeighbours(Cell centerCell) {

                int row = centerCell.getRow();
                int column = centerCell.getColumn();
                centerCell.getNeighbours().clear();
                for (int i = row - 1; i <= row + 1; i++) {
                    for (int j = column - 1; j <= column + 1; j++) {
                        try {
                            Cell neighbourCell = get(i).get(j);
                            centerCell.getNeighbours().add(Optional.of(neighbourCell));
                        } catch (IndexOutOfBoundsException ex) { }
            }
        }
    }

    private void countAllNeibourMines() {
        emptyCellStream().forEach(cell -> cell.countNeighbourMines());
    }

    public void click(int i, int j) throws Exception {
        if (isDead) throw new Exception("Dead ones never bite");
        Cell cellToOpen = get(i).get(j);
        cellToOpen.onClick();
    }

    public void onDeath() {
        isDead = true;
    }

    public String getString(boolean showUnrevealedCells) {
        StringBuilder sb = new StringBuilder();
        stream().forEachOrdered(row -> {
            row.stream().forEachOrdered(cell -> {
                switch (cell.cheatyPeek()) {
                    case CELL: {
                        if (showUnrevealedCells) {
                            sb.append(String.format("[%d]", ((EmptyCell)cell).getCount()));
                        } else {
                            sb.append(cell.isRevealed() ? String.format("[%d]", ((EmptyCell)cell).getCount()) : "[?]");
                        }
                    } break;
                    case MINE: {
                        if (showUnrevealedCells) {
                            sb.append("[X]");
                        } else {
                            sb.append("[?]");
                        }
                    }
                }
            });
            sb.append("\n");
        });
        return sb.toString();
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getROWS() {
        return ROWS;
    }

    public int getCOLUMNS() {
        return COLUMNS;
    }

    public boolean isDead() {
        return isDead;
    }

    @Override
    public String toString() {
        return getString(true);
    }

    public Stream<Cell> cellStream() {
        Stream.Builder<Cell> sb = Stream.builder();
        forEach(cells -> cells.forEach(cell -> sb.add(cell)));
        return sb.build();
    }

    public Stream<EmptyCell> emptyCellStream() {
        return cellStream()
                .filter(cell -> cell.cheatyPeek() == CellType.CELL)
                .map(cell -> (EmptyCell) cell);
    }

    public Stream<MineCell> mineCellStream() {
        return cellStream()
                .filter(cell -> cell.cheatyPeek() == CellType.MINE)
                .map(cell -> (MineCell) cell);
    }
}
