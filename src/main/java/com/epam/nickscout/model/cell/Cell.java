package com.epam.nickscout.model.cell;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
public abstract class Cell {
    protected int row;
    protected int column;
    protected boolean isRevealed;
    protected List<Optional<Cell>> neighbours;


    public Cell(int row, int column) {
        this.row = row;
        this.column = column;
        neighbours = new ArrayList<>(8);
        isRevealed = false;
    }

    public abstract CellType onClick();

    public abstract CellType cheatyPeek();

    public List<Optional<Cell>> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<Optional<Cell>> neighbours) {
        this.neighbours = neighbours;
    }

    public void addNeighbour(Cell cell) {
        neighbours.add(Optional.of(cell));
    }

    public boolean isRevealed() {
        return isRevealed;
    }

    public int getRow() {

        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public Stream<Cell> neigboursStream() {
        return neighbours.stream()
                .filter(cell -> cell.isPresent())
                .map(cell -> cell.get());
    }

    public Stream<EmptyCell> neigbourEmptyCellStream() {
        return neigboursStream()
                .filter(cell -> cell.cheatyPeek() == CellType.CELL)
                .map(cell -> (EmptyCell) cell);
    }

    public Stream<MineCell> neigbourMineCellStream() {
        return neigboursStream()
                .filter(cell -> cell.cheatyPeek() == CellType.MINE)
                .map(cell -> (MineCell) cell);
    }
}
