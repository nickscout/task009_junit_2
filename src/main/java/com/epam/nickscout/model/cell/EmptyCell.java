package com.epam.nickscout.model.cell;

import com.epam.nickscout.controller.Board;
import lombok.Getter;

@Getter
public class EmptyCell extends Cell {

    private int count;

    public void countNeighbourMines() {
        count = (int) neigbourMineCellStream().count();
    }

    public EmptyCell(int row, int column) {
        super(row, column);
        this.count = 0;
    }

    @Override
    public CellType onClick() {
        isRevealed = true;
        int score = Board.getInstance().getScore() + 5;
        Board.getInstance().setScore(score);
        return CellType.CELL;

    }

    @Override
    public CellType cheatyPeek() {
        return CellType.CELL;
    }

    public int getCount() {
        return count;
    }
}
