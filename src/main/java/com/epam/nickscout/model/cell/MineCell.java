package com.epam.nickscout.model.cell;

import com.epam.nickscout.controller.Board;

public class MineCell extends Cell {
    public MineCell(int row, int column) {
        super(row, column);
    }

    @Override
    public CellType onClick() {
        Board.getInstance().onDeath();
        return CellType.MINE;
    }

    @Override
    public CellType cheatyPeek() {
        return CellType.MINE;
    }
}
