package com.epam.nickscout.view;

import com.epam.nickscout.controller.Board;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Console {
    public static Logger logger = LogManager.getLogger();

    public static int play() {
        Board board = Board.getInstance();
        while (!board.isDead()) {
            Scanner sc = new Scanner(System.in);
            int i, j;
            logger.info("\n" + board.getString(false));
            System.out.print("i : ");
            i = sc.nextInt();
            System.out.print("j : ");
            j = sc.nextInt();
            try {
                board.click(i, j);
            } catch (Exception e) {
                break;
            }
        }
        logger.info("lol, you died!");
        logger.info("\n" + board.getString(true));
        return board.getScore();
    }
}
